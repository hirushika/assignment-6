/*202092-Wickramaarachchi H.P
	Assignment-6(Question1) */
	
//program to print number pattern

#include<stdio.h>

int pattern(int number);
//function to print the pattern

int pattern(int number)
{
    int i;
    
    if(number==0)
    
        return 0;
        
    else
    {
        pattern(number-1);
        for(i=number; i>=1; i--)
        
            printf("%d ",i);
            printf("\n");
            
              
        }
}


int main()
{
 
    int number;
    
    printf("Enter number of rows in the pattern : ");
    scanf("%d",&number);
    
    pattern(number);
 
    return 0;
 
}

