/*202092-Wickramaarachchi H.P
	Assignment-6 (Question-2)*/
	
//program to print fibonacci sequence according to given limit

#include <stdio.h>

int fibseq(int);
//function to print fibonacci series

int fibseq(int i) {
	
		if(i == 0) {
	    	return 0;
	   }
		
	   if(i == 1) {
	    	return 1;
	   }
	   
	   return fibseq(i-1) + fibseq(i-2);
}

int  main() {

    int i,number;
    
	printf("Enter limit of the series : ");
	scanf("%d",&number);
	
    for (i = 0; i < number+1; i++) {
    	printf("%d\n", fibseq(i));
   }
	
   return 0;
}
